FROM nginx:1.19-alpine

ENV FASTCGI_PORT="9000" \
  FASTCGI_HOST="qgisserver" \
  HTTPS="off" \
  SERVER_NAME="localhost" \
  SERVER_PORT="80"

COPY default.conf.template /etc/nginx/templates/
